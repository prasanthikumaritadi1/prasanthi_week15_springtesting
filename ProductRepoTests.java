package com.gl.Springboottesting;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.Rollback;

import com.gl.Entity.Product;
import com.gl.Repository.Repo;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@TestMethodOrder(OrderAnnotation.class)
public class ProductRepoTests {

	@Autowired
	private Repo repo;
	
	@Test
	@Rollback(false)
	@Order(1)
	public void testCreateProduct() {
		Product savedProduct = repo.save(new Product("Ryzen 9", 500));
		
		assertThat(savedProduct.getproductId()).isGreaterThan(0);
	}
	
	@Test
	@Order(2)
	public void testFindProductByName() {
		Product product = repo.findByName("Ryzen 9");		
		assertThat(product.getproductName()).isEqualTo("Ryzen 9");
	}
	
	@Test
	@Order(3)
	public void testListProducts() {
		List<Product> products = (List<Product>) repo.findAll();		
		assertThat(products).size().isGreaterThan(0);
	}	
	
	@Test
	@Rollback(false)
	@Order(4)
	public void testUpdateProduct() {
		Product product = repo.findByName("Ryzen 9");
		product.setproductPrice(1000);
		
		repo.save(product);
		
		Product updatedProduct = repo.findByName("Ryzen 9");
		
		assertThat(updatedProduct.getproductPrice()).isEqualTo(1000);
	}
	
	@Test
	@Rollback(false)
	@Order(5)
	public void testDeleteProduct() {
		Product product = repo.findByName("Ryzen 9");
		
		repo.deleteById(product.getproductId());
		
		Product deletedProduct = repo.findByName("Ryzen 9");
		
		assertThat(deletedProduct).isNull();		
		
	}
	
}
