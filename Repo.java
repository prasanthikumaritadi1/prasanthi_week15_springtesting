package com.gl.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.gl.Entity.Product;

public interface Repo extends JpaRepository<Product, Integer> {
	
	public Product findByName(String name);
}

